<?php
//si on est dans le répertoire SITE alors on redirige à la racine
//echo $_SERVER["REQUEST_URI"];exit();
if (substr($_SERVER["REQUEST_URI"],0,5)=='/atd/')
{
  $url='http://'.$_SERVER["SERVER_NAME"].'/'.substr($_SERVER["REQUEST_URI"],5);
  //  echo $url;
  header("Location: $url");
  exit();
}


$mysql_debug = true;

$type_urls = "propres2";
$quota_cache = 50;

$GLOBALS['debut_intertitre'] = "<h2>";
$GLOBALS['fin_intertitre'] = "</h2>";
# Et on y ajoute quatre nouvelles :
$GLOBALS['debut_intertitre_2'] = '<h3>';
$GLOBALS['fin_intertitre_2'] = '</h3>';
$GLOBALS['debut_intertitre_3'] = '<h4>';
$GLOBALS['fin_intertitre_3'] = '</h4>';

function avant_propre($texte) {

	$chercher_raccourcis = array(
#		/*  1 */ "/\{0\{/",
#		/*  2 */ "/\}0\}/",
		/*  3 */ "/\{1\{/",
		/*  4 */ "/\}1\}/",
		/*  5 */ "/\{2\{/",
		/*  6 */ "/\}2\}/",
		/*  7 */ "/\{3\{/",
		/*  8 */ "/\}3\}/",
		/*  9 */ "/[[:space:]]--[[:space:]]/",
		/* 10 */ "/[[:space:]]--,[[:space:]]/",
		/* 11 */ "/\{§\{/",
		/* 12 */ "/\}§\}/",
		/* 13 */ "/<-->/",
		/* 14 */ "/-->/",
		/* 15 */ "/<--/",
        /* 16 */ "/#<>#/",
        /* 17 */ "/#<#/",
        /* 18 */ "/#>#/",
        );

	$remplacer_raccourcis = array(
#		/*  1 */ "@@SPIP_debut_intertitre_0@@",
#		/*  2 */ "@@SPIP_fin_intertitre_0@@",
		/*  3 */ "@@SPIP_debut_intertitre@@",
		/*  4 */ "@@SPIP_fin_intertitre@@",
		/*  5 */ "@@SPIP_debut_intertitre_2@@",
		/*  6 */ "@@SPIP_fin_intertitre_2@@",
		/*  7 */ "@@SPIP_debut_intertitre_3@@",
		/*  8 */ "@@SPIP_fin_intertitre_3@@",
		/*  9 */ " &mdash; ",
		/* 10 */ " &mdash;, ",
		/* 11 */ "<span style=\"font-variant: small-caps\">",
		/* 12 */ "</span>",
		/* 13 */ "&harr;",
		/* 14 */ "&rarr;",
		/* 15 */ "&larr;",
  	    /* 16 */  "<div style=\"clear:both\">&nbsp;</div>",
  	    /* 17 */  "<div style=\"clear:left\">&nbsp;</div>",
  	    /* 18 */  "<div style=\"clear:right\">&nbsp;</div>",

			);

	$texte = preg_replace($chercher_raccourcis, $remplacer_raccourcis, $texte);

	return $texte;
}

function apres_propre($texte) {

	# Le remplacement des intertitres de premier niveau a déjà été effectué dans inc_texte.php3

	# Intertitre de deuxième niveau
	global $debut_intertitre_2, $fin_intertitre_2;
	if(!$debut_intertitre_2) $debut_intertitre_2 = "<h4 class=\"spip\">";
	if(!$fin_intertitre_2) $fin_intertitre_2 = "</h4>";
	$texte = ereg_replace('(<p class="spip">)?[[:space:]]*@@SPIP_debut_intertitre_2@@', $debut_intertitre_2, $texte);
	$texte = ereg_replace('@@SPIP_fin_intertitre_2@@[[:space:]]*(</p>)?', $fin_intertitre_2, $texte);

	# Intertitre de troisième niveau
	global $debut_intertitre_3, $fin_intertitre_3;
	if(!$debut_intertitre_3) $debut_intertitre_3 = "<h5 class=\"spip\">";
	if(!$fin_intertitre_3) $fin_intertitre_3 = "</h5>";
	$texte = ereg_replace('(<p class="spip">)?[[:space:]]*@@SPIP_debut_intertitre_3@@', $debut_intertitre_3, $texte);
	$texte = ereg_replace('@@SPIP_fin_intertitre_3@@[[:space:]]*(</p>)?', $fin_intertitre_3, $texte);

		$cherche1 = array(
			/* 19 */ 	"/\[\(\-/",  //encadrer float left
			/* 20 */	"/\-\)\]/",
			/* 19 */ 	"/\[\(\+/",  //encadrer float right
			/* 20 */	"/\+\)\]/",
			/*  ec1 */ 	"/\[\-/",  //centrer
			/*  ec2 */	"/\-\]/",
			/* 15 */ 	"/\[\+/",  //droite
			/* 16 */	"/\+\]/",
			/* 17 */ 	"/\[\=/",  //justifié
			/* 18 */	"/\=\]/",
			/* 19 */ 	"/\[\(/",  //encadrer
			/* 20 */	"/\)\]/",
			/* 19 */ 	"/\[\.(.*)\(/",  //encadrer avec option
			/* 20 */	"/\)\]/",
			/* 21 */ 	"/\[\*/",
			/* 22 */	"/\*\]/",
			/* 23 */ 	"/\[\^/",
			/* 24 */	"/\^\]/",
			/* 25 */    	"/<p class=\"spip\"><ul class=\"spip\">/",
			/* 26 */	"/<\/ul>( *)<\/p>/",
			/* 27 */    	"/<p class=\"spip\"><ol class=\"spip\">/",
			/* 28 */	"/<\/ol>( *)<\/p>/",
			/* 29 */    	"/<p class=\"spip\"><table class=\"spip\">/",
			/* 30 */	"/<\/table>( *)<\/p>/",
			/* 31 */    	"/<p class=\"spip\">(\n| *)<div/",
			/* 32 */	"/<\/div>( *)<\/p>/",
			/* 33 */	"/<p class=\"spip\"><h([0-9])>/",
			/* 34 */	"/<\/h([0-9])>( *)<\/p>/",
			/* 35 */    	"/<table cellpadding=5 cellspacing=0 border=0 align=''> <tr><td align='center'> <div class='spip_documents'>/",
			/* 36 */    	"/<\/div> <\/td><\/tr> <\/table>/",
			/* 37 */    	"/<p class=\"spip\"><div/",
			/* 38 */    	"/<p class=\"spip\"><blockquote class=\"spip\">/",
			/* 39 */    	"/<\/blockquote>( *)<\/p>/",
			/* ec1 */	"/\[color=(#?[[:alnum:]]*)\]/", //couleur
      		/* ec2 */	"/\[\/color\]/",
			/* ec3 */	"/\[bgcolor=(#?[[:alnum:]]*)\]/", //couleur de fond
      		/* ec4 */	"/\[\/bgcolor\]/",
			/* ec5 */	"/\[size=([\+|\-]?[[:alnum:]]*)\]/", //taille
      		/* ec6 */	"/\[\/size\]/",
			/*  ec7 */ 	"/\[\_/",  //souligne
			/*  ec8 */	"/\_\]/"



		);
		$remplace1 = array(
			/* 19 */ 	"<div class=\"texteencadre-left\">",
			/* 20 */	"</div>",
			/* 19 */ 	"<div class=\"texteencadre-right\">",
			/* 20 */	"</div>",
			/*  ec1 */ 	"<div style=\"text-align:center;\">",
			/*  ec2 */	"</div>",
			/* 15 */ 	"<div style=\"text-align:right;\">",
			/* 16 */	"</div>",
			/* 17 */ 	"<div style=\"text-align:justify;\">",
			/* 18 */	"</div>",
			/* 19 */ 	"<div class=\"texteencadre-spip\">",
			/* 20 */	"</div>",
			/* 19 */ 	"<div class=\"\${1}\">",
			/* 20 */	"</div>",
			/* 21 */ 	"<span class=\"caractencadre-spip\">",
			/* 22 */	"</span>",
			/* 23 */ 	"<sup>",
			/* 24 */	"</sup>",
			/* 25 */	"<ul class=\"spip\">",
			/* 26 */	"</ul>",
			/* 27 */	"<ol class=\"spip\">",
			/* 28 */	"</ol>",
			/* 29 */	"<table class=\"spip\">",
			/* 30 */	"</table>",
			/* 31 */	"<div",
			/* 32 */	"</div>",
			/* 33 */	"<h$1>",
			/* 34 */	"</h$1>",
			/* 35 */	"<div class='spip_documents'>",
			/* 36 */	"</div>",
			/* 37 */	"<div",
			/* 38 */    	"<blockquote class=\"spip\"><p class=\"spip\">",
			/* 39 */    	"</p></blockquote>",
			/*  ec1 */	"<font color=\"\${1}\">",
		   /*  ec2 */	"</font>",
			/*  ec3 */	"<span style=\"background-color:\${1}\">",
		   /*  ec4 */	"</span>",
  			/*  ec5 */	"<font size=\"\${1}\">",
		   /*  ec6 */	"</font>",
  			/*  ec7 */	"<u>",
		   /*  ec8 */	"</u>"




		);
		$texte = preg_replace($cherche1, $remplace1, $texte);
	return $texte;
}

/*pour lien virtuel
changer dans ecrire/inc/texte.php
function typer_raccourci ($lien) {

	if (!preg_match(_RACCOURCI_URL, trim($lien), $match)) return false;
	$f = $match[1];
	// valeur par defaut et alias historiques
	if (!$f) $f = 'article';
	else if ($f == 'art') $f = 'article';
	else if ($f == 'virt') $f = 'virtuel'; //ajout ec
	else if ($f == 'br') $f = 'breve';
	else if ($f == 'rub') $f = 'rubrique';
	else if ($f == 'aut') $f = 'auteur';
	else if ($f == 'doc' OR $f == 'im' OR $f == 'img' OR $f == 'image' OR $f == 'emb')
		$f = 'document';
	else if (preg_match(',^br..?ve$,S', $f)) $f = 'breve'; # accents :(
	$match[0] = $f;
	return $match;
}
*/
/*
include_spip('urls/propres2');
function generer_url_virtuel($id_article) {
        $id_virtuel=$GLOBALS['id_article'];
        if (!$id_virtuel) //lien n'est pas dans un article
	  if ($GLOBALS['id_rubrique']) //essaie rubrique
  	  {
            $s = spip_query("SELECT id_article FROM spip_articles WHERE statut='publie' and id_rubrique=$GLOBALS[id_rubrique] order by titre limit 0,1");
   	    $row = spip_fetch_array($s);
	    $id_virtuel=$row['id_article'];
	  }

        if (!$id_virtuel)
	  $id_virtuel=$id_article; //si on a rien trouvéon garde l'article courant... en gros on ne fait rien

	$url = _generer_url_propre('article', $id_article);
	if ($url)
		return _debut_urls_propres . $url . _terminaison_urls_propres.'?id_virtuel='.$id_virtuel;
	else
		return get_spip_script('./')."?page=article&id_article=$id_article&id_virtuel=".$id_virtuel;
}

function calculer_url_virtuel($id, $texte='') {
	$lien = generer_url_virtuel($id);
	$s = spip_query("SELECT titre,lang FROM spip_articles WHERE id_article=$id");
	$row = spip_fetch_array($s);
	if ($texte=='')
		$texte = supprimer_numero($row['titre']);
	return array($lien, 'spip_in', $texte, $row['lang']);
}
*/