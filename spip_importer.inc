<?php
/**
 * @file
 * The core importer process.
 *
 * This file group all the functions used during the import process.
 */

define('INCLUDE_ARTICLES', 0);
define('EXCLUDE_ARTICLES', 1);


/**
 * function to import spip articles into drupal
 */
function _spip_importer_import($form_datas) {
  // Already imported articles id for ANY content types
  $already_imported = array();
  $result = db_query("SELECT field_spip_importer_aid_value FROM {field_data_field_spip_importer_aid}");
  foreach ($result as $record) {
    $already_imported[] = $record->field_spip_importer_aid_value;
  }

  $args = array();
  $where = '';

  // Add rubrique id to sql condition
  if ($form_datas->rubrique_id) {
    $args[':rid'] = $form_datas->rubrique_id;
    $cond = ' id_rubrique = :rid ';
    $where .= $where ? "AND $cond" : $cond;
  }

  // add articles ids to include or to exclude (if we want to import a rubrique)
  if (! empty($form_datas->articles_ids)) {
    switch ($form_datas->inc_or_exc) {
      case EXCLUDE_ARTICLES:
        $cond = ' id_article not in (:aids) ';
        $where = $where ? "AND $cond" : $cond;
        break;
      case INCLUDE_ARTICLES:
        $cond = ' id_article in (:aids) ';
        $where = $where ? "AND $cond" : $cond;
        break;
    }
    $args[':aids'] = $form_datas->articles_ids;
  }

  if (! $where) {
    drupal_set_message(t('No citerias found to import articles.'), 'error');
    return NULL;
  }

  $fields = "id_article, surtitre, soustitre,	descriptif, chapo, texte, ps, nom_site, url_site, url_propre, lang, id_trad";
  $query = "SELECT $fields FROM {articles} WHERE $where";
  $result = _spip_importer_query($query, $args);

  $nb_total = 0;
  $nb_imported = 0;
  $nb_translation = 0;

  $drupal_languages = language_list();

  foreach ($result as $row) {
    $nb_total++;

    $id_article = $row->id_article;

    // If it is not the original article but a translation we get the original
    if ($row->id_trad && $id_article != $row->id_trad) {
      $query = "SELECT $fields FROM {articles} WHERE id_article = :tid";
      $args = array(':tid' => $row->id_trad);
      $origine_article = _spip_importer_query($query, $args)->fetchObject();

      if (in_array($origine_article->lang, array_keys($drupal_languages))) {
        $id_article = $origine_article->id_article;
        $row = $origine_article;
      }
    }

    // If article is not already imported
    if (array_search($id_article, $already_imported) === FALSE) {

      if (! in_array($row->lang, array_keys($drupal_languages))) {
        drupal_set_message(t('Language %code for article %aid not supported in this site', array('%code' => $row->lang, '%aid' => $id_article)), 'error');
        continue;
      }

      $imported = _spip_importer_import_article($row, $form_datas->tid);

      if ($imported) {
        $nb_imported++;

        $node = $imported['node'];
        $spip_data = $imported['spip_data'];

        foreach ($spip_data->traduction as $translation_id) {
          if ($translation_id != $id_article && array_search($translation_id, $already_imported) === FALSE) {
            $query = "SELECT $fields FROM {articles} WHERE id_article = :aid";
            $article = _spip_importer_query($query, array(':aid' => $translation_id))->fetchObject();
            if (! in_array($article->lang, array_keys($drupal_languages))) {
              drupal_set_message(t('Language %code for translation %tid of article %aid not supported in this site', array('%code' => $article->lang, '%aid' => $id_article, '%tid' => $translation_id)), 'error');
              continue;
            }
            if (isset($node->translations->data) && in_array($article->lang, array_keys($node->translations->data))) {
              // This language is already imported we can not importing it again
              drupal_set_message(t('SPIP article %tid could not be imported as a translation of article %oid since it is the second version of this article in the same language. It is imported as a separate article.', array('%tid' => $translation_id, '%oid' => $spip_data->id_article)), 'warning');
              $other_import = _spip_importer_import_article($article, $form_datas->tid);
              continue;
            }
            $translation = _spip_importer_import_article($article, $form_datas->tid, $node);
            if ($translation) {
              $nb_translation++;
            }
          }
        }
      }
    }
    else {
      drupal_set_message(t("Article %aid was already imported and won't be imported again", array('%aid' => $id_article)), 'warning');
    }
  } // foreach ($result as $row)

  drupal_set_message(t("%imported articles imported on %total, %tanslations translations.", array('%imported' => $nb_imported, '%total' => $nb_total, '%tanslations' => $nb_translation)), 'status');
}

function _spip_importer_import_article($article, $tid, $translation_source = NULL) {
  $id_article = $article->id_article;
  $spip_data = _spip_importer_fetch('id_article', $id_article);
  if ($spip_data === FALSE) {
    drupal_set_message(t('No data to import for article @id', array('@id' => $id_article)), 'warning');
    return NULL;
  }

  global $user;

  $format_id = variable_get('spip_importer_filter_format', 'filtered_html');

  $title = $spip_data->titre;
  $title = preg_replace('@^\s*\d*\.\s*(.*)$@is', '$1', $title);
  $summary = ($spip_data->introduction && (! preg_match('@\[\s*;@is', $spip_data->introduction))) ? str_replace('(…)', '', $spip_data->introduction) : '';
  $summary = check_markup($summary, $format_id, $spip_data->lang);
  $body = _spip_importer_format_body($spip_data, $article, $format_id);
  $field_lang = variable_get('spip_importer_translation_method', 'field') == 'field' ? $spip_data->lang : LANGUAGE_NONE;

  if ($translation_source == NULL || $field_lang == LANGUAGE_NONE) {
    $node = (object) array(
      'uid'     => $user->uid,
      'type'    => variable_get('spip_importer_content_type', 'article'),
      'title'   => $title,
      'body'    => array($field_lang => array(0 => array(
        'value' => $body,
        'summary' => $summary,
        'format'  => $format_id,
      ))),
      'created' => $spip_data->date,
      'changed' => $spip_data->date_modif,
      'comment' => 1, // (1 = no further comments)
      'language' => $spip_data->lang,
      'revision' => 0,
    );

    if (isset($translation_source) && isset($translation_source->nid)) {
      $node->translation_source = $translation_source;
    }

    _spip_importer_set_status($spip_data->statut, $node);

    $handler = null;
  }
  else {
    $node = $translation_source;

    $handler = entity_translation_get_handler('node', $node);

    $node->body[$field_lang][0] = array(
      'value' => $body,
      'summary' => $summary,
      'format'  => $format_id,
    );
  }

  _spip_importer_set_logo($node, $spip_data->logo, $field_lang);

  _spip_importer_attach_documents($node, $article, $field_lang);

  $content_type = variable_get('spip_importer_content_type', 'article');

  // Set the title in title_field if exists (mostly used when entity_translation is used for translation)
  $field = field_info_instance('node', 'title_field', $content_type);
  if ($field) {
    $node->{$field['field_name']}[$field_lang][0]['value'] = $title;
  }

  // @todo rendre ce champ administrable !
  // Set the subtitle in subtitle_field if exists
  $field = field_info_instance('node', 'field_subtitle', $content_type);
  if ($field) {
    $node->{$field['field_name']}[$field_lang][0]['value'] = $spip_data->soustitre;
  }

  // Set the taxonomy to this node
  $field = field_info_instance('node', variable_get('spip_importer_taxonomy_field', ''), $content_type);
  $node->{$field['field_name']}[LANGUAGE_NONE][]['tid'] = $tid;

  // Attach informations about this SPIP article to the node
  $field = field_info_instance('node', 'field_spip_importer_aid', $content_type);
  $node->{$field['field_name']}[$field_lang][]['value'] = $id_article;

  // If a source of an image is still a SPIP source, mark the status field to "To check"
  $tocheck = preg_match('#src=["\'][^"\']*IMG#', $node->body[$field_lang][0]['value']);
  // If there's PRE, SCRIPT, STYLE, OBJECT or IFRAME tags : we'll probably have to check
  if (! $tocheck) {
    $tocheck = preg_match('#(?:<pre|<script|<style|<object|<iframe|<!--)#', $spip_data->texte);
  }
  // Check the urls
  if (! $tocheck) {
    preg_match_all('#href=["\']([^"\']*)["\']#', $node->body[$field_lang][0]['value'], $matches);
    foreach ($matches[1] as $value) {
      // If it is not an external or an anchor link it is a link to the SPIP site
      if (! url_is_external($value) && strpos($value, '#') !== 0) {
        $tocheck = TRUE;
        continue;
      };
    }
  }
  $field = field_info_instance('node', 'field_spip_importer_status', $content_type);
  $node->{$field['field_name']}[$field_lang][]['value'] =  $tocheck ? 'tocheck' : 'unchecked';

  if ($handler != NULL) {
    $translation = array(
      'translate' => 0,
      'status' => 1,
      'language' => $field_lang, // here is the language we're translating to
      'source' => $node->language, // here is the source language
    );
    $handler->setTranslation($translation, $node);
  }

  // Save the node.
  node_save($node);

  if (! isset($node->nid)) {
    drupal_set_message(t("Article %aid couldn't be saved to a drupal node", array('%aid' => $id_article)), 'warning');
    return NULL;
  }

  // If redirect module is enabled
  if (function_exists('redirect_object_prepare')) {
    $redirect = new stdClass();
    $defaults = array(
      'source' => "$article->url_propre.html",
      'redirect' => "node/$node->nid",
    );
    redirect_object_prepare($redirect, $defaults);
    redirect_save($redirect);
  }
  $langs = language_list();
  if ($field_lang != LANGUAGE_NONE) {
    $url_options = array('language' => $langs[$field_lang]);
  }
  else {
    $url_options = array('language' => $langs[$node->language]);
  }
  drupal_set_message(t('Article %aid imported to : <a href="@url">%title</a>', array('%aid' => $id_article, '@url' => url('node/' . $node->nid, $url_options), '%title' => $node->title_field[$field_lang][0]['value'])), 'status');

  return array('node' => $node, 'spip_data' => $spip_data);
}

/**
 * Format the drupal body with SPIP article datas.
 * If introduction is the begining of text it is not (re)included in the drupal body.
 *
 * @param string $spip_data : SPIP datas with introduction, texte, notes, ...
 * @param string $format_id : drupal text format for the body
 */
function _spip_importer_format_body($spip_data, $article, $format_id = 'filtered_html') {

  // Delete all newline so regexp will be easier and check_markup won't add unnecessary paragraph
  $intro = str_replace(array("\r\n", "\n", "\r"), ' ', $spip_data->introduction);
  $text = str_replace(array("\r\n", "\n", "\r"), ' ', $spip_data->texte);
  $notes = str_replace(array("\r\n", "\n", "\r"), ' ', $spip_data->notes);

  if ($article->nom_site && $article->url_site) {
    $intro = ($intro ? "$intro\n\n" : "") . "<strong>$article->nom_site</strong> : $article->url_site";
  }
  // If introduction is not in text add it to text
  if ($intro && strpos($spip_data->texte, substr($spip_data->introduction, 4, 94)) === FALSE && (! preg_match('@\[\s*;@', $spip_data->introduction))) {
    $text = "$intro\n\n$text";
  }
  // Add the notes if any
  if ($notes) {
    $text = "$text\n\n$notes";
  }
  if ($spip_data->soustitre && ! field_info_instance('node', 'field_subtitle', variable_get('spip_importer_content_type', 'article'))) {
    $text = "<h2>$spip_data->soustitre</h2>\n\n$text";
  }

  $text = str_replace('’', "'", $text);
  $text = str_replace('“', '"', $text);
  $text = str_replace('”', '"', $text);
  $text = str_replace('…', '...', $text);
  $text = str_replace('–', '', $text);

  $text = utf8_decode($text);

  // Reformat not well formatted img src
  $text = preg_replace("@(<img[^>]*src=)[^\"']*[\"']([^\"']+)[\"']@is", '$1"$2"', $text);

  // XXX test des images : 2282, 2112, 2485, 1597, 1136, 1090, 2768
  // replace img by img with the good classes
  if (preg_match_all('#<(span|div)[^>]*class=[\'"][^\'"]*(?:spip_documents_(left|right|center)[^\'"]*)?[\'"][^<]*(?:<a[^<]*type="image[^<]*)?<img[^>]*(src=[\'"][^\'"]*[\'"])[^<]*(?:</a[^<]*<div[^>]*class=[\'"]spip_doc_titre[\'"][^>]*>(.*)</div>[^<]*)?</\1>#Uis', $text, $matches, PREG_SET_ORDER)) {
    foreach ($matches as $match) {
      $style = $match[2];
      $src = $match[3];
      $title = isset($match[4]) ? $match[4] : FALSE;
      $img = "<img $src " . ($style ? " _class_=\"img-$style\" " : '') . ($title ? (' title="' . strip_tags($title) . '" ') : '') . "/>";
      $pattern = preg_quote($match[0], '#');
      $text = preg_replace("#$pattern#is", $img, $text);
    }
  }

  // @todo récupération des mp3.
  // XXX Test avec : 2063, 1600, 1582, 2424, 2527
  // XXX Au milieu de plein de trucs (fichiers à télécharger, images) : 1571,
  // XXX mp3 multiples : 1757, 1685
  // XXX .wav téléchargeable non écoutable : 2178
  // XXX photos + en pied de page : 2291
  // XXX mixcloud: 2641, 2470 (mixcloud multiple)
  // XXX RCF : 2770, 2772
  if (preg_match_all('#<div[^>]*class=[\'"][^\'"]*spip_documents[^\'"]*[\'"][^<]*(?:<div[^>]*class=[\'"]spip_doc_titre[\'"][^>]*>(.*)</div>[^<]*)?<div[^>]*class=[\'"]spip_doc_descriptif[\'"].*dewplayer\.swf\?son=([^\'"]+)[\'"].*(?:<div[^>]*id="aidemp3".*</div>)?.*</div>[^<]*</div>#Uis', $text, $matches, PREG_SET_ORDER)) {
    foreach ($matches as $match) {
      // @todo : bientôt
      $audio_file = $match[2];
      $audio_description = $match[1] ? strip_tags($match[1]) : $audio_file;
      $pattern = preg_quote($match[0], '#');
      $link = '<a href="' . $audio_file . '">' . $audio_description . '</a>';
      $text = preg_replace("#$pattern#is", $link, $text);
    }
  }

  // Replace objects/iframe by corresponding url if available
  // XXX articles test video : 1972, 1721, 1746, 1847, 1950, 1893, 2136, 1990, 2551
  preg_match_all('@<(object|iframe|script).*</\1>@Uis', $text, $matches);
  foreach ($matches[0] as $embed_elem) {
    if (preg_match('@vimeo\.com[^"]*(?:clip_id=|/)(\d+)@i', $embed_elem, $matches2)) {
      $text = preg_replace('@' . preg_quote($embed_elem, '@is') . '@', 'http://vimeo.com/' . $matches2[1], $text);
    }
    elseif (preg_match('@youtube\.com/(?:watch[#\?]v=|embed/|v/|\?v=)([^\'"]+)@i', $embed_elem, $matches2)) {
      $text = preg_replace('@' . preg_quote($embed_elem, '@is') . '@', 'http://youtu.be/' . $matches2[1], $text);
    }
    elseif (preg_match('@dailymotion\.com/(?:swf/video/|embed/video/)([^"\&/_]+)@i', $embed_elem, $matches2)) {
      $text = preg_replace('@' . preg_quote($embed_elem, '@is') . '@', 'http://dailymotion.com/video/' . $matches2[1], $text);
    }
    elseif (preg_match('@dailymotion\.com/(?:swf/|video/)([^"\&/_]+)@i', $embed_elem, $matches2)) {
      $text = preg_replace('@' . preg_quote($embed_elem, '@is') . '@', 'http://dailymotion.com/video/' . $matches2[1], $text);
    }
  }

  $dom = new DOMDocument('1.0', 'utf-8');
  if (@$dom->loadHTML($text)) {
    $xpath = new DOMXPath($dom);

    // Remove attachments (documents are in a separate field) Let's this field display it.
    $nodes_nodiv = _spip_importer_remove_elements($xpath, '//div[contains(@class, "spip_documents")]');
    // remove unnecessaries anchors
    $nodes_noanchor = _spip_importer_remove_elements($xpath, '//a[@name="visu"]');

    if ($nodes_nodiv || $nodes_noanchor) {
      // Delete all the things added by DOMDocument->loadHTML()
      $text = $dom->saveHTML();
      $text = preg_replace("#<!DOCTYPE [^>]+>#", "", $text);
      $text = preg_replace("#</?(?:html|body)[^>]*>#", "", $text);
    }
  }

  // Remove ' class="*"' from html : we don't know spip classes into drupal
  $text = preg_replace('@\s*class=[\'"][^\'"]*[\'"]@is', '', $text);
  // Replace wrong absolute url href for notes.
  $text = preg_replace('@(\s*href=)[\'"]http://[^/]+/(#[^\'"]+)[\'"]@is', '$1"$2"', $text);
  // Replace our own protected classes by html class attribute
  $text = str_replace('_class_', 'class', $text);

  return check_markup($text, $format_id, $spip_data->lang);
}

function _spip_importer_remove_elements(&$xpath, $query) {
  $elements = $xpath->query($query);
  if ($elements) {
    foreach ($elements as $node) {
      $node->parentNode->removeChild($node);
    }
  }

  return $elements;
}

/**
 * Executes a query on the spip database.
 *
 * The query is in drupal format, that is, you need to use {} around the table names.
 */
function _spip_importer_query($query, $args = array()) {
  _spip_importer_init_query();
  db_set_active('spip');
  $result = db_query($query, $args, array('target' => 'spip'));
  db_set_active('default');

  return $result;
}

function _spip_importer_init_query() {
  static $spip_db = NULL;

  if ($spip_db == NULL) {
    global $databases;
    // Get SPIP databases settings
    $db_name = variable_get('spip_importer_db_name', '');
    $db_user = variable_get('spip_importer_db_user', '');
    $db_pass = variable_get('spip_importer_db_pass', '');
    $db_host = variable_get('spip_importer_db_host', '');

    if (empty($db_name) || empty($db_user) || empty($db_host)) {
      drupal_set_message(t('At least one SPIP database setting is not set. Please go to the SPIP settings page to correct that.'), 'error', FALSE);
      return NULL;
    }

    $spip_db = array(
          'database' => $db_name,
          'username' => $db_user,
          'password' => $db_pass,
          'host' => $db_host,
          'port' => '',
          'driver' => 'mysql',
          'prefix' => 'spip_',
    );

    $databases['spip']['default'] = $spip_db;
    Database::parseConnectionInfo();
  }
}

/**
 * Connect to spip and fetch an object in json format.
 */
function _spip_importer_fetch($arg, $value) {
  $url = variable_get('spip_importer_url_site', '') . "/spip.php?page=json&$arg=$value&var_mode=calcul";

  $result = drupal_http_request($url);
  if (isset($result->error)) {
    form_set_error('', t('Unable to fetch: %url, error: %error, code: %code', array('%url' => $url, '%error' => $result->error, '%code' => $result->code)));
    return FALSE;
  }
  $spip_data = json_decode($result->data);
  if (!is_object($spip_data)) {
    form_set_error('', t('Empty result for: %url<br/>Dump:<pre>@dump</pre>', array('%url' => $url, '@dump' => var_export($result->data, TRUE))));
    return FALSE;
  }

  foreach ($spip_data as $key => $value) {
    if (is_string($value)) {
      $spip_data->$key = decode_entities($value);
    }
  }
  return $spip_data;
}

/**
 * Set the log if exists as an image field to the node
 *
 * @param object node the node object
 * @param string $logo_name logo file name
 */
function _spip_importer_set_logo(&$node, $logo_name, $field_lang) {
  // If there's a logo for this article we add it in the image field
  if ($logo_name) {
    // Get image field
    $field = field_info_instance('node', variable_get('spip_importer_logo_field', 'field_logo'), variable_get('spip_importer_content_type', 'article'));
    // And save image in it
    _spip_importer_save_file($node, variable_get('spip_importer_path_spip', '') . '/IMG/' . $logo_name, $field, $field_lang);
  }
}

/**
 * Attach a document to the node in a file field. If it is an image and the image is in the body, it only display this image to the body (it's not in a field).
 * @param $node Drupal node to insert
 * @param $text_spip SPIP article text
 * @param $id_article SPIP article id
 */
function _spip_importer_attach_documents(&$node, $article, $field_lang) {
  $spip_filesquery = "SELECT id_document FROM {documents_articles} WHERE id_article = :aid";
  $rsc = _spip_importer_query($spip_filesquery, array(':aid' => $article->id_article));

  // Put docs ids into an array so we will merge images or documents attached to another article later
  $id_documents = array();
  foreach ($rsc as $row) {
    $id_documents[$row->id_document] = $row->id_document;
  }

  // Merge images and docs from spip article with docs attached to spip article
  $text_spip = $article->descriptif . $article->chapo . $article->texte . $article->ps;
  preg_match_all('/<img(\d+)/', $text_spip, $img_matches);
  if ($img_matches[1]) {
    $id_documents += array_combine($img_matches[1], $img_matches[1]);
  }
  preg_match_all('/<doc(\d+)/', $text_spip, $doc_matches);
  if ($doc_matches[1]) {
    $id_documents += array_combine($doc_matches[1], $doc_matches[1]);
  }

  // We save the file to corresponding field.
  $field_document = field_info_instance('node', variable_get('spip_importer_documents_field', 'field_documents'), variable_get('spip_importer_content_type', 'article'));
  $node->{$field_document['field_name']}[$field_lang] = array();

  $field_images = field_info_instance('node', variable_get('spip_importer_images_field', 'field_images'), variable_get('spip_importer_content_type', 'article'));
  $node->{$field_images['field_name']}[$field_lang] = array();

  $field_audios = field_info_instance('node', variable_get('spip_importer_audios_field', 'field_audios'), variable_get('spip_importer_content_type', 'article'));
  $node->{$field_audios['field_name']}[$field_lang] = array();

  $filters = filter_list_format(variable_get('spip_importer_filter_format', 'filtered_html'));

  foreach ($id_documents as $id_document) {
    $spip_data = _spip_importer_fetch('id_document', $id_document);
    if ($spip_data === FALSE) {
      form_set_error('', t('Related article: %id_article', array('%id_article' => $article->id_article)));
      continue;
    }

    // Create managed File object.
    $uri = variable_get('spip_importer_path_spip', '') . '/' . $spip_data->fichier;

    // Get file extension
    preg_match('/\.([^.]+)\s*$/', $spip_data->fichier, $matches);
    $file_ext = strtolower($matches[1]);

    // If file extension is an allowed images field extension
    if (in_array($file_ext, explode(' ', strtolower($field_images['settings']['file_extensions'])))) {

      $file_name = preg_quote($spip_data->fichier);
      $is_in_body = preg_match_all("@<img[^>]*src=[\"'][^\"']*{$file_name}[\"'][^>]*>@", $node->body[$field_lang][0]['value'], $matches);
      $file = _spip_importer_save_file($node, $uri, $field_images, $field_lang, $is_in_body);

      // If file as been saved and image is in body, we update the body to show the newly saved image.
      if ($file && $is_in_body) {

        foreach ($matches as $match_img) {

          $img = $match_img[0];
          $attributes = '';

          if (preg_match("@class=[\"']([^\"']+)[\"']@", $img, $match_class)) {
            if ($match_class[1]) {
              $attributes = array('class' => $match_class[1]);
            }
          }

          if (isset($filters['media_filter'])) {
            $tagContent = array(
              'type' => 'media',
              'view_mode' => 'media_large',
              'fid' => $file->fid,
              'attributes' => $attributes,
            );
            $tag = '[[' . drupal_json_encode($tagContent) . ']]';

            $node->body[$field_lang][0]['value'] = preg_replace('#' . preg_quote($img) . '#', $tag, $node->body[$field_lang][0]['value']);
          }
          else {
            $node->body[$field_lang][0]['value'] = preg_replace('#' . preg_quote($img) . '#', '<img src="' . file_create_url($file->uri) . '" ' . (isset($attributes['class']) ? (' class="' . $attributes['class'] . '" ') : '') . '/>', $node->body[$field_lang][0]['value']);
          }
        }
      }
    }

    // If file extension is an allowed audios field extension
    elseif (in_array($file_ext, explode(' ', strtolower($field_audios['settings']['file_extensions'])))) {

      $file_name = preg_quote($spip_data->fichier);
      $is_in_body = preg_match_all('@<a href="([^"]*' . $file_name . ')">([^<]*)</a>@is', $node->body[$field_lang][0]['value'], $matches, PREG_SET_ORDER);
      $file = _spip_importer_save_file($node, $uri, $field_audios, $field_lang, 0);

      if ($file && $is_in_body) {
        foreach ($matches as $match) {
          $audio_element = $match[0];
          $spip_file = $match[1];
          $description = $match[2];
          $drupal_file = $file->uri;
          if (isset($filters['jplayer_filter'])) {
            $tag = '[jplayer#' . $drupal_file . ($description != $spip_file ? ('#' . $description) : '') . "]";
          }
          else {
            $tag = "<a href=\"$drupal_file\">$description</a>";
          }
          $node->body[$field_lang][0]['value'] = preg_replace('@' . preg_quote($audio_element, '@') . '@', $tag, $node->body[$field_lang][0]['value']);
        }
      }
    }

    // This is not an image and not an audio
    else {
      _spip_importer_save_file($node, $uri, $field_document, $field_lang);
    }

  } //foreach id_documents

  // And embed videos...
  if (preg_match_all('@<a href="([^"]*(?:dailymotion|vimeo|youtu)[^"]*)">\1</a>@is', $node->body[$field_lang][0]['value'], $matches, PREG_SET_ORDER)) {
    foreach ($matches as $match) {
      if ($embed_uri = $match[1]) {
        $embed_element = $match[0];
        _spip_importer_embed($node, $embed_element, $embed_uri, $field_images, $field_lang);
      }
    }
  }

}

function _spip_importer_embed(&$node, $embed_element, $embed_uri, $field, $field_lang) {
  try {
    $provider = media_internet_get_provider($embed_uri);
    $provider->validate();
    $file = $provider->save();

    // @todo mettre dans le bon field

    $tagContent = array(
      'type' => 'media',
      'view_mode' => 'media_large',
      'fid' => $file->fid,
      'attributes' => '',
    );
    $tag = '[[' . drupal_json_encode($tagContent) . ']]';
    $node->body[$field_lang][0]['value'] = preg_replace('@' . preg_quote($embed_element, '@') . '@', $tag, $node->body[$field_lang][0]['value']);
  }
  catch (Exception $e) {
    drupal_set_message($e->getMessage(),'error');
    return;
  }

}

/**
 * Save a file to the drupal fil system and attach it to the node field.
 *
 * @param Node $node : The node.
 * @param File $file : The file to save.
 * @param Field $field: : The field wheere to save the file.
 */
function _spip_importer_save_file(&$node, $uri, $field, $field_lang, $display = 1) {
  $not_created = TRUE;

  // Instantiate managed File object.
  $file = new stdClass();
  $file->uri = $uri;
  $file->filemime = file_get_mimetype($file->uri);
  $file->uid = $GLOBALS['user']->uid;

  $dir = 'public://' . $field['settings']['file_directory'];

  // Create dir if it does not exists (first use)
  if (file_prepare_directory($dir, FILE_CREATE_DIRECTORY)) {
    // Save the file to the directory defined in field
    if ($file = file_copy($file, $dir, FILE_EXISTS_REPLACE)) {

      // Don't attach again
      $already_imported = FALSE;
      if (isset($node->{$field['field_name']}[$field_lang])) {
        foreach ($node->{$field['field_name']}[$field_lang] as $already_present) {
          if ($already_present['uri'] == $file->uri) {
            $already_imported = TRUE;
            continue;
          }
        }
      }
      if (! $already_imported) {
        $node->{$field['field_name']}[$field_lang][] = array('fid' => $file->fid, 'uri' => $file->uri, 'filemime' => $file->filemime, 'display' => $display);
      }
      $not_created = FALSE;
    }
  }
  else {
    drupal_set_message(t("Directory %dir doesn't exist and/or is not writable", array('%dir' => $dir)), 'error');
    return FALSE;
  }
  if ($not_created) {
    drupal_set_message(t('File %file not created', array('%file' => $uri)), 'error');
    return FALSE;
  }

  return $file;
}

/**
 * Set the node status (published or not) based in spip statut.
 *
 * This is a separate in order to make it clean to override it and implement a
 * more complete moderation mechanism (for example see modr8).
 */
function _spip_importer_set_status($statut, &$node) {
  // calculate the article status
  switch ($statut) {
    // in the trash
    case 'poubelle':
      $node->status = 0;
      break;
      // published
    case 'publie':
      $node->status = 1;
      break;
      // draft (en cours de redaction)
    case 'prepa':
      $node->status = 0;
      break;
      // proposed to publish
    case 'prop':
      $node->status = 0;
      break;
      // refused
    case 'refuse':
      $node->status = 0;
      break;
  }
}

function _spip_importer_field_lang($node) {
  $langs = array_keys($node->body);
  return $langs[0];
}